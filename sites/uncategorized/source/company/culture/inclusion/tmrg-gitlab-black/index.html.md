---
layout: markdown_page
title: "TMRG - Black@GitLab"
description: "An overview of our remote TMRG Black@GitLab"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-black/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

## Mission

Promote personal and professional development. The overall goal is to position our current and prospective employees in positions such as managers, vice president, directors, etc. 
- Drive inclusion and engagement
- Evaluate current retention and hiring procedures
- Provide resources and foster a community

This group is open to all members of the GitLab community.

## Leads
* [April Marks](https://about.gitlab.com/company/team/#aprilmarks)
* [Marcus Carter](https://about.gitlab.com/company/team/#marcusbriancarter) 
* [Madou Coulibaly](https://about.gitlab.com/company/team/#madou)

## Executive Sponsors
* [David DeSanto](https://about.gitlab.com/company/team/#david)
* [Sherida McMullan](https://about.gitlab.com/company/team/#sheridam)
* [Liam McNally](https://about.gitlab.com/company/team/#lmcnally1)

## How to Join
* Please sign up [here](https://groups.google.com/a/gitlab.com/g/black-tmrg) to be added to the Black@GitLab TMRG Google Group. This will ensure you're added to upcoming TMRG calendar events. **It might take up to 24 hours before the calendar events appear on your calendar. If there is an event happening within 24 hours of you joining the Google Group, please post in the #black-at-gitlab  Slack channel to be added manually.**
* Join the [#black-at-gitlab](https://gitlab.slack.com/archives/C03KFLT0UJV) Slack channel for conversation, announcements, and connecting with other TMRG members. Please introduce yourself in the channel when you join!
* Join the [#reverse_ama_cro](https://gitlab.slack.com/archives/C029DTTFR0U) Slack channel for communication, transparency, feedback and tracking of actions about the Reverse AMA with Black Team Members.

## Member Involvement

This section is meant to outline how team members in the Black@GitLab TMRG can contribute to key initiatives each quarter. We encourage team members to contribute in any way they feel comfortable and have capacity - from meeting attendance to leading initiatives and anywhere in between!

### Monthly Black@GitLab TMRG Meeting

Every month, one meeting of 45 minutes will be hosted at a time to accomodate in turn US WEST/EAST, EMEA and APAC team members. Allies are always encouraged to attend any TMRG meeting. The call structure includes:
- 25 minutes for full group discussion of agenda topics
- 20 minutes social discussion (NOT RECORDED)

This meeting can be replaced by an edition special one with different structure to cover dedicated topics. 

### Quartely CRO Reverse AMA

This meeting is hosted 1x per month as follows
- every 2 months (odd) on the first Thursday at 01:00pm UTC and 01:45pm UTC.
- every 2 months (even) on the second Monday at 04:30pm UTC and 05:15pm UTC.

The purpose is to provide an opportunity to increase and leverage communication between our Sales Leadership and Sales team members who identify as people of color

Understanding and acknowledging that our team members of color are experiencing a lot of emotions right now, and are processing those emotions in different ways. Our [Values](https://about.gitlab.com/handbook/values/) promote being inclusive, [Building a Safe Community](https://about.gitlab.com/handbook/values/#building-a-safe-community), and [Being an Ally](https://about.gitlab.com/handbook/communication/ally-resources/), and we need to lean into these focus areas, starting with our own team. 

### Sponsorship Program

TBD

### Contribute to a TMRG key initiative

We use our [GitLab issue board](https://gitlab.com/gitlab-com/black-tmrg/-/boards) to coordinate and prioritize projects on a quarterly basis. Contribute to the issue board by:

1. Open a new issue to propose an idea or project
2. Contribute to an open issue that is assigned to a quarterly milestone. These milestones outline the current focus projects of the TMRG

## Events
- Sales Sponsorship program 2022
- Black Women's Equal Pay Day 2022
- Stock up on Stock Knowledge 2022
- Juneteenth Speaker with Tyrance Billingsley II 2022

## Additional Resources

- Black Author Book Club: select a book pertaining to career and leadership
- Encouraged to leverage the [Black Speaker Collection](https://blackspeakerscollection.com/) site to find a speaker for event
- Encouraged to leverage [Black in Tech Speakers] (https://www.allamericanspeakers.com/lists/black-in-tech.php)

### Black@GitLab TMRG logo 

TBD
